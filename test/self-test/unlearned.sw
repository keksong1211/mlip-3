Stillinger-Weber
species_count = 2
radial_basis_type = RBChebyshev
	scaling = 0.1
	min_val = 1.3
	max_val = 5.0
	basis_size = 6
